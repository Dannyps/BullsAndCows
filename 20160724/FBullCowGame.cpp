#include "FBullCowGame.h"


int FBullCowGame::GetMaxTries() const		{	return MyMaxTries; }

int FBullCowGame::GetCurrentTry() const		{	return MyCurrentTry; }

bool FBullCowGame::IsGameWon() const		{	return bIsGameWon; }

FBullCowGame::FBullCowGame()
{	Reset(); }

void FBullCowGame::Reset()
{
	MyCurrentTry = 1;
	MyMaxTries = 8;
	MyHiddenWord = "planet";
	bIsGameWon = 0;
	return;
}

EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess)
{
	
	if (!IsIsogram(Guess)) // if the guess is not an isogram
		return EGuessStatus::Not_Isogram; // return an error
	else if (!IsLowerCase(Guess)) // if the guess is not all lowercase
		return EGuessStatus::Not_Lowercase; // return an error
	else if (Guess.length() != GetHiddenWordLength()) // if the guess does not have the correct length
		return EGuessStatus::Wrong_Length; // return an error
	else return EGuessStatus::Ok;
		
}
// receives a valid guess, increments turn and returns count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	
	// increment the turn number
	MyCurrentTry++;
	// settup a return variable
	FBullCowCount BullCowCount;
	// loop through each leter in the hidden word and compare them against the guess
	int32 WordLength = MyHiddenWord.length();
	for (int32 i = 0; i < WordLength; i++) {
		for (int32 j = 0; j < WordLength; j++) {
			if (Guess[i] == MyHiddenWord[j]) {
				if (i == j)
					BullCowCount.Bulls++;
				else
					BullCowCount.Cows++;
			}
		}
	}
	if (BullCowCount.Bulls == WordLength)
		bIsGameWon = true;
	return BullCowCount;
}

int32 FBullCowGame::GetHiddenWordLength()
{
	return MyHiddenWord.length();
}

bool FBullCowGame::IsIsogram(FString Word) const
{
	if (Word.length() <= 1) return true;
	TMap<char, bool> LetterSeen;
	for (char Letter : Word) {
		Letter = tolower(Letter);
		if (LetterSeen[Letter]){
			return false;
		}
		else {
			LetterSeen[Letter] = true;
		}
	}
	return true;
}

bool FBullCowGame::IsLowerCase(FString Word) const
{
	for (char Letter : Word) {
		if (!islower(Letter)) {
			return false;
		}
	}
	return true;
}
