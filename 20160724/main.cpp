/* This is the console executabl thay makes use of the BullCow class.
This acts a the view in a MVC pattern, asn is responsible for all
user interaction. For game logic use the FBullCowGame class.
 */

#include <iostream>
#include <string>

#include "FBullCowGame.h"

using int32 = int;
using FText = std::string;
void printIntro();
bool askToPlayAgain();
void playGame();
void PrintGameSummary();
FText getValidGuess();
FBullCowGame BCGame;

// Entry point for our application
int main() {

	do {
		printIntro();
		playGame();
		
	} while (askToPlayAgain());


	return 0;
}

void playGame()
{
	BCGame.Reset();

	while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= BCGame.GetMaxTries()) {
		FText guess = getValidGuess();
		FBullCowCount BullCowCount = BCGame.SubmitValidGuess(guess);

		std::cout << "Bulls: " << BullCowCount.Bulls << ".\n";
		std::cout << "Cows: " << BullCowCount.Cows << ".\n\n";
		PrintGameSummary();
	}
}

void PrintGameSummary()
{
	if (BCGame.IsGameWon())
		std::cout << "You win!\n";
	else
		std::cout << "Better luck next time!\n";
}

void printIntro() {
	std::cout << "Guess the " << BCGame.GetHiddenWordLength() << " lettered isogram I'm thinking about.\n";
	return;
}

FText getValidGuess() {
	EGuessStatus Status = EGuessStatus::Wrong_Length;
	FText guess = "";
	do {

		std::cout << "Try " << BCGame.GetCurrentTry() << "/" << BCGame.GetMaxTries() << ". Input your suggestion: ";
		std::getline(std::cin, guess);
		Status = BCGame.CheckGuessValidity(guess);
		switch (Status)
		{
		case EGuessStatus::Wrong_Length:
			std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n";
			break;
		case EGuessStatus::Not_Isogram:
			std::cout << "Please enter an isogram.\n";
			break;
		case EGuessStatus::Not_Lowercase:
			std::cout << "Please enter a lowercase word.\n";
			break;

		default:

			break;
		}
	} while (Status != EGuessStatus::Ok);
	return guess;
};

bool askToPlayAgain() {
	std::cout << "Do you want to play again (y/n)? ";
	FText res = "";
	std::getline(std::cin, res);
	if (res[0] == 'Y' || res[0] == 'y') {
		return true;
	}
	else return false;
}